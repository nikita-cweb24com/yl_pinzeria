<?php $prefix='Пинца: ';?>
<section class="main" <?php
$img=cwfield::image($prefix.'Фоновая картинка');
if ($img) {
	?>style="background-image:url(<?php
		echo $img;
		?>)" <?php
}
?>>
<div class="container">
	<div class="main-content">
		<div class="main-about">
			<div class="about-item">
				<div class="item-icon">
					<span></span>
				</div>
				<div class="item-content">						
					<h2><?php ngtheme::t($prefix.'О пинце (заголовок)','О пинце');?></h2>
					<p>
						<?php ngtheme::t($prefix.'О пинце (текст)',
							'ПИНЦА® (от латинского Pinsere'.
								' –раскатывать, измельчать)'.
						' - авторское, инновационное блюдо, '.
						'созданное и запатентованное гуру'.
						' итальянской кухни маэстро Валентино Бонтемпи (Valentino Bontempi).');?>
					</p>
					<a href="<?php 
					echo cwfield::url($prefix.'Ссылка');
					?>"><?php ngtheme::e('Подробнее');?></a>
				</div>
			</div>
			<div class="about-item-list">
				<ul>
					<li>
						<span>
							<img src="<?php bloginfo('template_url');?>/img/rollingpin.png" alt="rollingpin">
						</span>
						<?php ngtheme::t($prefix.'Текст рядом со скалкой','Изготавливается из специальной смеси муки');?>
					</li>
					<li>
						<?php ngtheme::t($prefix.'Текст "72"','<span>
							72
						</span>
						Пинца выдерживается 72 часа');?>
					</li>
				</ul>
			</div>
		</div>
		<div class="chef-menu">
			<div class="chef-menu-title">
				<h2><?php ngtheme::t('Меню от Шеф-Повара');?></h2>
			</div>
			<div class="chef-menu-content">
				<ul>
					<?php ngtheme::slider('pinza');?>					
				</ul>
			</div>
		</div>
	</div>
</div>
</section>