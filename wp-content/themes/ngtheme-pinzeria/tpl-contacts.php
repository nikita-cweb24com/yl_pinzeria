<?php
/**
Template Name: Контакты
**/
get_header();
while (have_posts()) {
	the_post();
	?> <section class="contacts">
    <div class="container">
        <div class="contacts-block">
            <div class="contacts-address">
                <?php
                NGTheme::Slider('contacts');
                ?>                   
                 <div class="map2">
                <?php $map1=cwfield::text('Карта нижняя');
                if (!$map1 || strlen($map1)==0) {
                    $map1='<iframe src="https://yandex.ru/map-widget/v1/-/CBqwnPe-oC" width="100%" height="400" frameborder="0"></iframe>';
                } echo $map1;?>
            </div>
            </div>
            <div class="map">
                <?php $map2=cwfield::text('Карта верхняя');
                if (!$map2|| strlen($map2)==0){
                    $map2='<script type="text/javascript" charset="utf-8" async src="//api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ad68a215a0f348ee0af51e2338363e8399da144a31ba4ca19a054966a4afd1c90&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>';
                } echo $map2;?>
            </div>

        </div>
    </div>
    </section><?php
}
get_footer();