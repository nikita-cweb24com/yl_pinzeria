<footer>
	<div class="container">
		<div class="footer-content">
			<div class="footer-logo footer-col">
				<a href="<?php echo get_site_url();?>">
					<img src="<?php
					$img=cwfield::image('Лого нижнее');
					if ($img){ echo $img;} else {
						bloginfo('template_url');?>/img/logo.png<?php } ?>" alt="Pinzeria">
					</a>
				</div>
				<div class="footer-menu footer-col">
					<?php cwmenu::menu('Нижнее меню');?>
				</div>
				<div class="footer-soc-icon footer-col">
					<?php ngtheme::menuSocial();?>
				</div>
				<div class="footer-address footer-col">
					<ul>
						<?php ngtheme::slider('contacts','block_licontacts');?>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<?php

	 wp_footer();
	echo (cwfield::text('Коды счетчиков'));
	 ?>
</body>

</html>