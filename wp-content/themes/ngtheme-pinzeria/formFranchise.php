<?php $after=cwfield::text('Текст после отправки формы');
if (!$after) {
    $after='Thank you';
}
$after='<h1 class="submittedresult">'.$after.'</h1>';
?>
<form name="<?php ngtheme::e('Форма на странице франшизы');?>" data-cwform1='<?php
echo (json_encode(array('success'=>$after)));
?>'>
    <div class="form-group">
        <label for="name"><?php ngtheme::e('Ваше имя');?> <span>*</span></label>
        <input name="<?php ngtheme::e('Ваше имя');?>" type="text" id="name" required />
    </div>
    <div class="form-group">
        <label for="email"><?php ngtheme::e('Ваш e-mail');?> <span>*</span></label>
        <input type="email" id="email" name="<?php ngtheme::e('Ваш e-mail');?>" required/>
    </div>
    <div class="form-group">
        <label for="phone"><?php ngtheme::e('Номер телефона');?> <span>*</span></label>
        <input type="tel" id="phone" name="<?php ngtheme::e('Номер телефона');?>" required/>
    </div>
    <div class="form-group">
        <label for="message"><?php ngtheme::e('Сообщение');?> <span>*</span></label>
        <textarea id="message" name="<?php ngtheme::e('Сообщение');?>" required></textarea>
    </div>

    <div class="send-btn">
        <button type="submit"><?php ngtheme::e('Отправить');?></button>
    </div>
</form>