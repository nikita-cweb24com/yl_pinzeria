<?php
//$data = get_transient('nginstaphotos');
$data=false;
if (!$data) {
    //ob_start();
    echo do_shortcode('[instagram-feed num=16 cols=4 showfollow=false]');
  //  ob_get_clean();
}return;
//var_dump($data);
//$data = json_decode(urldecode(get_option('insta')), true);
set_transient('nginstaphotos', $data);
$images = array();
$count = 0;
?>
<ul>
    <?php
    foreach ($data['data'] as $row => $values) {

        if ($count == 16) {
            break;
        }
        $trans=$values['images']['thumbnail']['url'];
        
        
        $headers = (
                get_transient('nginsta'.crc32($trans))
                );
        if (!$headers) {
            $headers=get_headers($values['images']['thumbnail']['url']);
            set_transient('nginsta'.crc32($trans),$headers);
        }
        if (!strpos($headers[0], '200 OK')) {
            continue;
        }
        $count++;
        ?>
        <li>
            <a href="<?php
               echo $values['images']['standard_resolution']['url'];
               ?>" data-imagelightbox="instagram" data-ilb2-caption="<?php
           echo htmlspecialchars($values['caption']['text']);
           ?>">
                <img src="<?php echo $values['images']['thumbnail']['url'] ?>" alt="Pinzeria Instagram">
            </a>
        </li>
<?php } ?>	
</ul>
