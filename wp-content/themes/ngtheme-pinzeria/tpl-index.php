<?php 
/**
Template name: Главная
**/
get_header();
while(have_posts()) {
	the_post();
	$blocks=array('main','restaurants','pizzas','social');
	foreach ($blocks as $block) {
		get_template_part('index',$block);
	} 
	?>	
	<?php
}
get_footer();