<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title();?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <style>
   .post-type-archive-restaurant [class*="ymaps-2"][class*="-ground-pane"] {
        -webkit-filter: brightness(70%);
    }
    </style>
    <link rel="shortcut icon" href="<?php bloginfo('template_url');?>/favicon.ico" type="image/vnd.microsoft.icon" />
    <!-- Page css files -->
    <?php wp_head();?>
</head>
<body <?php body_class();?>>
    <header>
        <div class="header-top">
            <div class="container">
                <div class="top-content">
                    <div class="address">
                        <ul>
                            <?php ngtheme::slider('contacts','block_licontacts');?>
                        </ul>
                    </div>
                    <div class="logo">
                        <a href="<?php echo get_site_url();?>">
                            <img src="<?php
                            $img=cwfield::image('Лого верхнее');
                            if ($img){ echo $img;} else {
                             bloginfo('template_url');?>/img/logo.png<?php } ?>" alt="Pinzeria">
                        </a>
                    </div>
                    <div class="mobile-content">
                        <?php if (2==1) { ?>
                        <div class="user">
                            <a href="">
                                <span>
                                    <img src="<?php bloginfo('template_url');?>/img/mobile-user.png" alt="User">
                                </span>
                                Валентино
                            </a>
                        </div>
                        <div class="cart">
                            <a href="#">
                                <img src="<?php bloginfo('template_url');?>/img/mobile-cart.png" alt="shopping cart">
                                <span>45</span>
                            </a>
                        </div>
                        <?php } ?>
                        <div class="menu-btn">
                            <button id="toggleMenu">
                                <span class="text"><?php ngtheme::e('Меню');?></span>
                                <span class="icon">
                                    <span></span>
                                    <span class="item2"></span>
                                    <span></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="languages">
                        <?php ngtheme::lang();?>
                    </div>
                </div>
            </div>
            <div class="header-content">
                <div class="container">
                    <div class="mobile-menu">
                        <div class="mobile-nav">
                            <?php cwmenu::menu('Верхнее меню');?>
                        </div>
                        <div class="languages">
                            <?php ngtheme::lang();?>
                        </div>
                    </div>
                    <div class="content-block">
                        <div class="header-menu">
                            <?php cwmenu::menu('Верхнее меню');?>
                        </div>
                        <?php if (2==1) { ?>
                        <div class="header-user">
                            <ul>
                                <li class="user">
                                    <a href="#">
                                        <span>
                                            <img src="<?php bloginfo('template_url');?>/img/user.png" alt="User">
                                        </span>
                                        Валентино
                                    </a>
                                </li>
                                <li class="cart">
                                    <a href="#">
                                        <img src="<?php bloginfo('template_url');?>/img/shop-cart.png" alt="shopping cart">
                                        <span>45</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </header>