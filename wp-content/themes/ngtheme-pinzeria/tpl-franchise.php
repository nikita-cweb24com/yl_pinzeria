<?php
/**
  Template name: Франшиза
 * */
get_header();
while (have_posts()) {
    the_post();
    ?>
    <section class="franchise-main <?php
    if (isset($franchiseversion)) {
        ?> franchise-v2<?php
             }
             ?>">
        <div class="container">
            <?php
            $img = cwfield::image('Главная картинка');
            if ($img) {
                ?>
                <div class="main-img">
                    <img src="<?php echo $img; ?>" alt="<?php
                    the_title();
                    ?>">
                </div>
            <?php } ?>
            <div class="main-content">
                <?php
                //echo get_the_content();
                $cont = get_the_content();
                $cont = str_replace('<span id="more-' . get_the_id() . '"></span>', '<!--more-->', $cont);
                $content = explode('<!--more-->', ($cont));
                //print_r($content);die();
                echo $content[0];
                ?>
            </div>
        </div>
    </section>
    <section class="benefits">
        <div class="container">
            <div class="benefits-content">
                <?php ngtheme::benefitsImage(1); ?>
                <div class="benefits-text">
                    <?php echo $content[1]; ?>
                </div>
            </div>
            <div class="benefits-content right">
                <div class="benefits-text">
                    <?php echo $content[2]; ?>					
                    <?php
                    $quote = cwfield::text('Цитата');
                    if ($quote) {
                        ?>
                        <div class="quote">
                            <?php echo $quote; ?>
                        </div>
                    <?php } ?>
                        
                    
                </div>
                <?php ngtheme::benefitsImage(2); ?>
            </div>
        </div>
    </section>
<?php }
?><section class="franchise-form">
    <div class="container">
        <div class="form-block">
            <div class="form-title">
                <h3><?php ngtheme::e('Хотите работать с лучшими? Узнайте условия сотрудничества с нами!'); ?></h3>
            </div>
            <?php cwfb::form('formFranchise'); ?>
        </div>
    </div>
</section><?php
get_footer();
?>
