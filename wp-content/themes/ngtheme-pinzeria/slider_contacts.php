<div class="address-item address-item-<?php
 echo sanitize_title(
    get_the_title(pll_get_post(get_the_id(),'ru')
    )); ?>">
    <div class="address-title">
        <h3><?php the_title();?></h3>
    </div>
    <div class="address-content">
        <ul>
            <li class="tel">
                <h5><?php ngtheme::e('Телефон');?>:</h5>
                <?php echo cwfield::text('Номер телефона');?>
            </li>
            <?php if(cwfield::text('Адрес')) { ?>
            <li>
                <h5><?php ngtheme::e('Адрес');?>:</h5>
                <?php echo cwfield::text('Адрес');?>
            </li>
            <?php } ?>
            <li class="time">
                <h5><?php ngtheme::e('Время работы');?>:</h5>
                <?php echo cwfield::text('Время работы');?>
            </li>
        </ul>
    </div>
</div>