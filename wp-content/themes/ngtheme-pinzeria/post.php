<?php get_header();?>
<section class="pinza">
	<div class="container">
		<div class="pinza-content">
			<?php while (have_posts()) {
				the_post();?>
				<div class="pinza-title">
					<h2><?php the_title();?></h2>
				</div>
				
				<?php
				the_content();
			} ?>
		</div>
	</div>
</section>
<?php get_footer();