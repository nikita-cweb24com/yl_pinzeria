<?php

$prefix='Рестораны:';
$images=cwfield::files($prefix.'Фотографии');

?>
<section class="restaurants">
	<div class="container">
		<div class="restaurants-block">
			<div class="restaurants-content">
				<div class="content-top">
					<?php if (isset($images[0])) {
						?>
						<img src="<?php echo $images[0]['url'];?>" alt="<?php echo $images[0]['fields']['title'];?>">
						<?php } else { ?>
						<img src="<?php bloginfo('template_url');?>/img/resttop.png" alt="restaurants">
						<?php } ?>	</div>
						<div class="content-bottom">
							<div class="bottom-left">
								<div class="left-img">
									<?php if (isset($images[1])) {
										?>
										<img src="<?php echo $images[1]['url'];?>" alt="<?php echo $images[0]['fields']['title'];?>">
										<?php } else { ?>
										<img src="<?php bloginfo('template_url');?>/img/restleft.jpg" alt="restaurants">
										<?php } ?>
									</div>
									<div class="restaurants-btn">
										<a href="<?php
										echo get_post_type_archive_link('restaurant');
										?>"><?php ngtheme::e('ПОДРОБНЕЕ');?></a>
									</div>
								</div>
								<div class="bottom-text">

									<h2><?php ngtheme::t($prefix.'Заголовок','Рестораны');?></h2>
									<?php ngtheme::t($prefix.'(текст)','
									<p>
										Pinzeria® by Bontempi – это новый проект от именитого шеф-повара Валентино Бонтемпи.
									</p>
									<p>
										Концепция ресторана – уникальна в своем роде, и объединяет вкусную еду, итальянскую атмосферу и умеренные цены. В меню помимо
										основного блюда - пинцы® - также представлены потрясающие салаты, супы, эксклюзивные горячие
										блюда от шефа и широкий ассортимент авторских десертов, а также блюда дня – написанные на
										доске.
									</p>
									<p>
										Главное блюдо здесь - ПИНЦА®, аналогов которой нет на российском рынке. Это инновационный продукт для рынка пиццы, главной
										особенностью которого являются высокое качество и усвояемость.
									</p>');?>
								</div>
							</div>
						</div>
						<div class="valentino-bontempi">
							<div class="valentino-img">
								<?php $prefix='Валентино:';
$img=cwfield::image($prefix);
if (!$img){
								?>
								<img src="<?php bloginfo('template_url');?>/img/valentino.jpg" alt="Valentino Bontempi">
			<?php } else { ?>
			<img src="<?php echo $img;?>" alt="<?php
ngtheme::t($prefix.'Валентино Бонтемпи (заголовок)','Валентино Бонтемпи');
			 ?>"><?php } ?>
							</div>
							<div class="valentino-text">
								<h3><?php 
								ngtheme::t($prefix.'Валентино Бонтемпи (заголовок)','Валентино Бонтемпи');
								?></h3>
								<p><?php ngtheme::t($prefix.'Цитата',
									'«Я бережно отношусь к здоровью и считаю, что блюдо должно быть в первую очередь полезным. Благодаря своей технологии приготовления
									пинца® содержит очень низкое количество глютена, что делает используемое тесто практически безвредным
									для организма. Здоровая пища с качественными ингредиентами – это то, чем мы руководствуемся при
									приготовлении блюд»');?>
								</p>
								<div class="restaurants-btn">
									<a href="<?php 
echo cwfield::url($prefix.'Ссылка');
									?>"><?php ngtheme::e('ПОДРОБНЕЕ');?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>