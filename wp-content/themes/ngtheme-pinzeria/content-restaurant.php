<?php if (!get_the_excerpt() || !strlen(get_the_excerpt())) {
wp_update_post(array(
'ID'=>get_the_id(),
'post_excerpt'=>'Москва, Большой Знаменский пер., д. 2, стр. 3',

    ));
update_post_meta(get_the_id(),'_thumbnail_id',82);
}?>
<div class="restaurants-item">
    <div class="item-img">
        <a href="<?php the_permalink();?>">
            <img src="img/restaurants/item1.jpg" alt="Our restaurants">
        </a>
    </div>
    <div class="item-content">
        <h4>
            <a href="<?php the_permalink();?>"><?php the_title();?></a>
        </h4>
        <p><?php the_excerpt();?></p>
    </div>
</div>