<?php 
/**
Template name: Ресторатор
**/
get_header();
while (have_posts()) {
    the_post();
ngtheme::sliderAttach('Слайды');
?> <section class="valentino">
        <div class="container">
            <div class="valentino-block">
                <div class="valentino-content">
                    <div class="valentino-text">
                       <?php the_content();?>
                    </div>
                    <div class="quote"><?php echo cwfield::text('Цитата');?></div>
                </div>
                <?php $img=cwfield::image('Фотография внизу');                
                if ($img) { ?>
                <div class="valentino-img">
                    <img src="<?php echo $img;?>" alt="<?php the_title();?>">
                </div>
                <?php } ?>
            </div>
        </div>
    </section><?php }
get_footer();