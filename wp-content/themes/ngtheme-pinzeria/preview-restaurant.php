<div class="restaurants-item">
    <div class="item-img">
        <a href="<?php the_permalink();?>">
            <?php the_post_thumbnail('rest');?>
        </a>
    </div>
    <div class="item-content">
        <h4>
            <a href="<?php the_permalink();?>"><?php the_title();?></a>
        </h4>
        <?php the_excerpt();?>
    </div>
</div>