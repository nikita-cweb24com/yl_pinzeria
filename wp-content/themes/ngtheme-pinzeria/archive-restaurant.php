<?php
get_header();
//$taxonomies = get_object_taxonomies( array( 'post_type' => get_post_type() ) );
$terms=(get_terms('type'));
$args=array();
foreach ($terms as $term) {
	$args[$term->name]=array(
		'post_type'=>get_post_type(),
		'posts_per_page'=>-1,
		'tax_query'=>array(array(
			'taxonomy'=>'type',
			'field'=>'slug',
			'terms'=>$term->slug
		))
	);
}
$coordinates=array();
while (have_posts()){
	the_post();

	cwfield::text("Координаты:широта");
	cwfield::text("Координаты:долгота");
	
	if (cwfield::text("Координаты:широта") && cwfield::text("Координаты:долгота")) {
		$coordinates[get_the_title()]=array(
			(float)cwfield::text("Координаты:широта"),
			(float)cwfield::text("Координаты:долгота")
		);
	}
}

?>;
<div class="restaurants">
	<?php foreach ($args as $title=>$query){
		wp_reset_query();

		$posts=new WP_QUERY($query);
		if (!$posts->have_posts()) {
			continue;
		}
		?>

		<section class="restaurants-block">
			<div class="container">

				<div class="restaurants-content">
					<div class="restaurants-title">
						<h2><?php echo $title;?></h2>
					</div>

					<div class="restaurants-items">
						<?php 
						while ($posts->have_posts()) {
							$posts->the_post();
							get_template_part('preview',get_post_type());
							wp_reset_query();
						} ?>
					</div>

				</div>

			</div>
		</section>
		<?php
		wp_reset_query();
	}  ?>
</div>
<div id="map" style="width: 100%; height: 500px"></div>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript">
	ymaps.ready(init);
	<?php if (empty($coordinates)){?>
		var coords = [[55.745934, 37.602517], [55.731551, 37.644261], [42.888832, 47.634749],[48.662599, 44.444014], [59.937107, 30.322147]];
		<?php } else {?>
			var coords=<?php echo json_encode(array_values($coordinates));?>;
			<?php } ?>
			function init() {
				var myMap = new ymaps.Map("map", {
					<?php $center=cwfield::text('Центр карты - координаты через запятую');
					if (!$center) {$center='55,37';}?>
					center: [<?php echo $center;?>],
					zoom: <?php
					$zoom=cwfield::text('Приближение карты (4 по умолчанию)'); if (!$zoom){$zoom=4;} echo $zoom;?>,
					behaviors: ["drag", "dblClickZoom", "rightMouseButtonMagnifier", "multiTouch"],           

					clusterize: true,
					controls: [],
				},{suppressMapOpenBlock: true});
				var zoomControl = new ymaps.control.ZoomControl({
					options: {
						size: "large"
					}
				});
				myMap.controls.add(zoomControl);

	//	myMap.behaviors.disable('scrollZoom');
	for (i = 0; i < coords.length; ++i) {
		myPlacemark = new ymaps.Placemark(coords[i], {}, {
			iconLayout: 'default#image',
			iconImageHref: '<?php bloginfo('template_url');?>/img/map_marker.png',
			iconImageSize: [35, 46],
			iconImageOffset: [-17, -46]
		});
		myMap.geoObjects.add(myPlacemark);
	}
}
</script>

<?php get_footer();?>