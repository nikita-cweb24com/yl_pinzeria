$(document).ready(function() {
  $('a[data-imagelightbox="instagram"]').imageLightbox({
    activity: !0,
    caption: !0,
    button: !0,
    overlay: !0
  }),
    $(".owl-carousel").owlCarousel({
      loop: !0,
      margin: 0,
      nav: !0,
      items: 1,
      navText: ["<span></span>", "<span></span>"],
      dots: !0,
      responsive: { 0: { nav: !1 }, 576: { nav: !0 } }
    }),
    $("#toggleMenu").click(function() {
      $(this).toggleClass("active"), $(".mobile-menu").toggleClass("active");
    });
});