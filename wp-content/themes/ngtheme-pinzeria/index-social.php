<?php
$prefix="Виджеты соцсетей:";
$data=get_option('insta');
//print_r(json_decode(urldecode($data),true));
$blocks=array('facebook'=>'pinzeria-fb-content',
'instagram'=>'pinzeria-instagram-img','tripadvisor'=>'ta-logo hr');?>

<section class="pinzeria-social">
	<div class="container">
		<div class="social-block">
			<?php foreach ($blocks as $block=>$value) {?>
			<div class="pinzeria-<?php echo $block;?>">
				<div class="pinzeria-social-title">
					<h3><?php ngtheme::t($prefix.$block.' '.'Заголовок','#pinzeria в '.$block);?>
					</h3>
				</div>
				<div class="<?php echo $value;?>">
					<?php
					//var_dump(locate_template('index-social-'.$block.'.php'));
					if(strlen(locate_template('index-social-'.$block.'.php'))) {
						get_template_part('index-social-'.$block);
					} else {
					echo (cwfield::text($prefix.$block.' (код)'));
					}
					?>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
