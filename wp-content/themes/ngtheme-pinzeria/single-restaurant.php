<?php 
get_header();
while (have_posts()){ the_post();
	ngtheme::sliderAttach('Картинки в слайдер');
	?>
	<section class="restaurant">
		<div class="container">
			<div class="restaurant-block">
				<div class="restaurant-content">
					<div class="content-title">
						<h2><?php the_title();?></h2>
					</div>
					<div class="content-text">
						<?php the_content();?>
					</div>
					<?php if (2==1) {?>
					<div class="content-address">
						<?php echo cwfield::text('Адрес');?>
					</div>
					<?php } ?>
					<div class="content-btn">
						<?php
						$menu=cwfield::files('Меню ресторана');
						if (!$menu|| empty($menu)) { } else {?>
						<a href="<?php echo $menu[0]['url'];?>"><?php ngtheme::e('Посмотреть меню ресторана');?></a>
						<?php } ?>
					</div>

				</div>
				<?php $map=cwfield::text('Код карты');
				if ($map) {
					?>
					<div class="map">
						<?php echo ($map);?>
					</div>
					<?php } ?>
				</div>
			</div>
			</section><?php }
			get_footer();