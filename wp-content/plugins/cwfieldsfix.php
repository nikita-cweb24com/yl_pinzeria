<?php
/**
  Plugin Name: cwfields fix reloaded
  Plugin URI:
  Description: create menus easilly
  Author: Nikita Menshutin
  Version: 1.0
  Author URI: http://cweb24.com

  PHP version 5.5.9
 *
  @category Cweb24
  @package  Cweb24
  @author   Nikita Menshutin <nikita@cweb24.com>
  @license  http://cweb24.com commercial
  @link     http://cweb24.com
 * */
defined('ABSPATH') or die("No script kiddies please!");

Class NGFieldsFix
{
    function __construct()
    {
        if (!get_option('oldfiles')) {
            update_option('oldfiles', get_option('cwfiles'));
        }
        if (!get_option('oldfields')) {
            update_option('oldfields', get_option('cwfields'));
        }
        if (isset($_GET['ngfields1'])) {
            update_option('cwfieldbypass', true);
            add_action('init', array($this,$_GET['ngfields1']));
            add_action('init', array($this,'afterfix'));
        }
    }

    function afterfix()
    {
        delete_option('cwfieldbypass');
        die('OK');
    }

    function _lang($field)
    {
        $lang='[ru_RU]';
        if (isset($field['name'])) {
            $name=$field['name'];
        } else {
            $name=$field['title'];
        }

        if (strpos('_'.$name, '[en_GB]')) {
            $lang='[en_GB]';
        }
        return ($lang);
    }

    function attachments()
    {
        $files=get_option('cwfiles');

        foreach ($files['typical'] as $pt=> $fields) {
            foreach ($fields as $field) {
              $files_=json_decode(get_post_meta($field['pid'],'attachments',true),true);
              if (!isset($files_[$field['sysname']]) || empty($files_[$field['sysname']])) {
                unset(
                $files['typical'][$field['post_type']][$field['sysname']]
                );
                
              }
            }
        }
        update_option('cwfiles',$files);
    }

    function files()
    {
        $this->files=get_option('cwfiles');
        $this->postfiles=array();
        foreach ($this->files['typical'] as $pt=>$fields) {
            foreach ($fields as $field) {
                $this->postfiles[$field['pid']][$field['title']]=$field;
            }
            foreach ($fields as $field) {
                $this->filesProcess($field);
            }
        }
    }

    function filesProcessVars($field)
    {
        $lang=$this->_lang($field);
        $files_=json_decode(get_post_meta($field['pid'], 'attachments', true), true);
        if (!isset($field['name'])) {
            $field['name']=$field['title'];
        }

        if (!isset($files_[$field['sysname']])) {
            if ($files_) {
            }
            return;
        }
        $files=$files_[$field['sysname']];
        if (!isset($field['name'])) {
            $field['name']=$field['name'];
        }
        $new=$field;
        $new['name']=str_replace($lang, '', $new['name']);
        $names=explode(':', $new['name']);

        if (count($names)!=2) {
            $names=array('others', $new['name']);
        }
        $names[0].=$lang;

        $pages=get_page_by_title($names[0], OBJECT, 'cwfieldsvars');

        if (empty($pages)) {
            $id=wp_insert_post(
                array('post_title'=>$names[0],
                'post_type'=>'cwfieldsvars',
                'post_status'=>'publish'
                )
            );
        } else {
            $id=$pages->ID;
        }
        unset($names[0]);
        $new['name']=implode(':', $names);
        $new['title']=$new['name'];
        $new['files']=$files;
        $new['sysname']=cwfield::sanitize($new['name']);
        $newfiles=json_decode(get_post_meta($id, 'attachments', true), true);
        if (!$newfiles) {
            $newfiles=array();
        }
        $newfiles[$new['sysname']]=$files;
        echo $id;
        echo "\n";
        echo $field['name'];
        echo "\n";
        update_post_meta($id, 'attachments', json_encode($newfiles, JSON_UNESCAPED_UNICODE));
    }

    function filesProcess($field)
    {
        if ($field['post_type']=='cwfieldsvars') {
            return $this->filesProcessVars($field);
        }
        $lang=pll_get_post_language($field['pid'], 'locale');
        if ($lang=='en_GB') {
            return $this->filesProcessEn($field);
        }
        $files_=json_decode(get_post_meta($field['pid'], 'attachments', true), true);
        if (!isset($field['name'])) {
            $field['name']=$field['title'];
        }

        if (!isset($files_[$field['sysname']])) {
            if ($files_) {
            }
            return;
        }
        $files=$files_[$field['sysname']];
        if (!isset($field['name'])) {
            $field['name']=$field['name'];
        }
        $sanitize_title='cwfiles'.cwfield::sanitize($field['name']);
        $files_[$sanitize_title]=$files;
        echo $field['pid']."\n";
        echo $field['name']."\n";
        echo $sanitize_title."\n";
        update_post_meta($field['pid'], 'attachments', json_encode($files_, JSON_UNESCAPED_UNICODE));
    }

    function filesProcessEn($field)
    {
        $trans=pll_get_post($field['pid'], 'ru');
        if (!$trans || !isset($this->postfiles[$trans])) {
            return;
        }
        if (!isset($field['name'])) {
            $field['name']=$field['title'];
        }
        foreach ($this->postfiles[$trans] as $tfield) {

            $field['sysname']=$tfield['sysname'];
            $files=json_decode(get_post_meta($field['pid'], 'attachments', true), true);
            if (!isset($files[$field['sysname']])) {
                continue;
            }
            $sanitize_title='cwfiles'.cwfield::sanitize($field['name']);
            $files[$sanitize_title]=$files[$field['sysname']];
            update_post_meta($field['pid'], 'attachments', json_encode($files, JSON_UNESCAPED_UNICODE));
            echo $field['pid']."\n";
            echo $field['name']."\n";
            echo $sanitize_title."\n";
        }
    }

    function fields()
    {
        $options=array();
        $fields=get_option('cwfields');
        $old=get_option('oldfields');
        if (!$old) {
            update_option('oldfields', $fields);
        }
        $old=get_option('oldfields');
        foreach ($old['typical']['cwfieldsvars'] as $field) {
            $new=$field;
            $lang=$this->_lang($field);

            $new['name']=str_replace($lang, '', $new['name']);
            $names=explode(':', $new['name']);

            if (count($names)!=2) {
                $names=array('others', $new['name']);
            }
            $names[0].=$lang;

            $pages=get_page_by_title($names[0], OBJECT, 'cwfieldsvars');

            if (empty($pages)) {
                $id=wp_insert_post(
                    array('post_title'=>$names[0],
                    'post_type'=>'cwfieldsvars',
                    'post_status'=>'publish'
                    )
                );
            } else {
                $id=$pages->ID;
            }
            unset($names[0]);
            $new['name']=implode(':', $names);
            $new['sysname']=cwfield::sanitize($new['name']);
            //$new['lang']=$lang;
            $new['value']=get_post_meta($field['id'], $field['sysname'], true);

            //print_r($new);
            update_post_meta(
                $id,
                $new['sysname'],
                $new['value']
            );
            $new['id']=$id;
            $options[$new['sysname']]=$new;
            //$new['lang']=$lang;
            //print_r($new);

        }
        $fields['typical']['cwfieldsvars']=$options;
        update_option('cwfields', $fields);
    }

}

new NGFieldsFix();
if (isset($_GET['ngfields'])) {
    $files=get_option('cwfiles');
    $oldfiles=get_option('oldfiles');
    if (!$oldfiles) {
        update_option('oldfiles', $files);
    }
    $oldfiles=get_option('oldfiles');
    foreach ($oldfiles['typical'] as $pt=>$fields) {
        foreach ($fields as $field) {
            $lang='[ru_RU]';
            if (strpos('_'.$field['title'], '[en_GB]')) {
                $lang='[en_GB]';
            }
            if ($pt!='cwfieldsvars') {
                $files_=json_decode(get_post_meta($field['pid'], 'attachments', true), true);
                if (isset($files_[$field['sysname']])) {
                    //  $oldfiles['typical'][$pt][$field['sysname']]['value']=$files[$field['sysname']];
                    echo $field['title']."\n";
                    echo $field['pid']."\n";
                    echo $lang;
                    echo "\n";
                    $newfiles=$files_;

                    echo '!!!'."\n";
                }
            }

        }
    }
    //print_r($oldfiles);
    die('OK');

}
?>
